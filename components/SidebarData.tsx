import {
    AiFillCaretDown,
    AiFillCaretUp
} from 'react-icons/ai';
import {GiSittingDog,GiCat} from 'react-icons/gi'
import { FcHome} from 'react-icons/fc';
import { BiCategoryAlt} from 'react-icons/bi';
import { IoIosAnalytics} from 'react-icons/io';
import { SidebarItem } from '../models/SidebarItem';

export const SidebarData: SidebarItem[] = [
    {
        title: 'Home',
        path: '/',
        icon: <FcHome />
    },
    {
        title: 'Types',
        path: '/',
        icon: <BiCategoryAlt />,
        iconClosed: <AiFillCaretDown />,
        iconOpened: <AiFillCaretUp />,
        subnav: [
            {
                title: 'Dogs',
                path: '/types/dogs',
                icon: <GiSittingDog />
            },
            {
                title: 'Cats',
                path: '/types/cats',
                icon: <GiCat />
            }
        ]
    },
    {
        title: 'Chart',
        path: '/charts/areaChart',
        icon: <IoIosAnalytics/>
    }
];