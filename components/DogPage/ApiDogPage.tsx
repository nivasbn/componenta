import React,{useState,useEffect} from "react"
import { IDogPage } from "@/API/models/IDogPage"
import { DogPageService } from "@/API/service/DogPageService"
import MiniCards from "./MiniCards";

interface IState{
    loading: boolean;
    users: IDogPage[];
    errorMsg: string;
}

const ApiDogPage = () =>{

   const [state,setState]=useState<IState>({
    loading: false,
    users: [] as IDogPage[],
    errorMsg: ''
   })

   useEffect(() => {
    setState({ ...state, loading: true })
    DogPageService.getAllUsers()
        //   .then((res)=>console.log(res.data))
        .then((res) => setState({
            ...state, loading: false, users: res.data
        }))
        .catch(err => setState({
            ...state, loading: false, errorMsg: err.message
        }))
// Eslint-disable-next-line
}, [])

const {loading, users, errorMsg} = state

return(
    <>
    {users.length > 0 && users.map((user)=>{
        return(
            <>
 <MiniCards id={user.id} name={user.name} image={user.image} content={user.content}/>
            </>
        )
    })}
   
    </>
)

} 
export default ApiDogPage