import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

interface MiniCardsProps{
  id: number;
  name: string;
  image: string;
  content: string;
}

const MiniCards=(props:MiniCardsProps)=> {
  return (
    <Card sx={{ maxWidth: 300, display:'flex' ,flexDirection:"row",marginRight:3}} key={props.id}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image={props.image}
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {props.name}
          </Typography>
          <Typography variant="body2" color="text.secondary">
           {props.content}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
export default MiniCards