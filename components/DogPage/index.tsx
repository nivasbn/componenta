import React from 'react'
import MiniCards from './MiniCards'
import DogData from "./DogData.json"
import CssBaseline from '@mui/material/CssBaseline'
import { Box, Container } from '@mui/material'
const DogPage = () => {
console.log(DogData)


    return (
        <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xl">
        <Box sx={{ bgcolor: '#f0f5ed' , display: 'flex', flexDirection: 'row', marginLeft:"200px",marginTop:"60px"}} >
        {DogData.map((d, index) => {
                return (
                    <MiniCards id={index} name={d.name} image={d.image} content={d.content} />
                )
            })}
        
        </Box>
            </Container>
            </React.Fragment>

        
    )
}

export default DogPage

//https://mocki.io/v1/cb554982-b768-4edd-ba43-2ad534f507b7