import React,{useState,useEffect} from "react"
import sanityClient from "../../client"
import MiniCards from "../DogPage/MiniCards"

const CatPage=()=> {
    const [projectData, setProjectData]=useState(null)

    useEffect(() => {
        sanityClient.fetch(`*[_type == "dogPage"]{
            id,
            name,
            image,
            content
        }`)
            .then((data) => setProjectData(data))
            .catch(console.error)

    }, [])
return(
    <>
    {projectData && projectData.map((project)=>{
        return(
            <>
        
<MiniCards id={project.id} name={project.name} image={project.image} content={project.content}/>
            </>
        )
    })}
    
    </>
)
}
export default CatPage