import React, { useState, useEffect } from "react"
import { ICatPage } from "@/API/models/ICatPage"
import { CatPageService } from "@/API/service/DogPageService"
import MiniCards from "../DogPage/MiniCards";
import { Box, Container, CssBaseline } from "@mui/material";

interface IState {
    loading: boolean;
    users: ICatPage[];
    errorMsg: string;
}

const ApiCatPage = () => {
    const [state, setState] = useState<IState>({
        loading: false,
        users: [] as ICatPage[],
        errorMsg: ''
    })

    useEffect(() => {
        setState({ ...state, loading: true })
        CatPageService.getAllUsers()
            //   .then((res)=>console.log(res.data))
            .then((res) => setState({
                ...state, loading: false, users: res.data
            }))
            .catch(err => setState({
                ...state, loading: false, errorMsg: err.message
            }))
        // Eslint-disable-next-line
    }, [])
    const { loading, users, errorMsg } = state
    return (
        <React.Fragment>
        <CssBaseline />
        <Container maxWidth="xl">
          <Box sx={{ bgcolor: '#f0f5ed' , display: 'flex', flexDirection: 'row', marginLeft:"200px",marginTop:"60px"}} >
         
            {users.length > 0 && users.map((user) => {
                return (
                    <>
                        <MiniCards id={user.id} name={user.name} image={user.image} content={user.content} />
                    </>
                )
            })}

</Box>
            </Container>
            </React.Fragment>
    )
}

export default ApiCatPage