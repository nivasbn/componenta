import React from 'react'
import MiniCards from '../../components/DogPage/MiniCards'
import CatData from "./CatData.json"
import CssBaseline from '@mui/material/CssBaseline'
import { Box, Container } from '@mui/material'
const CatPages= () => {
console.log(CatData)


    return (
        <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xl">
        <Box sx={{ bgcolor: '#f0f5ed' , display: 'flex', flexDirection: 'row', marginLeft:"200px",marginTop:"60px"}} >
        {CatData.map((d, index) => {
                return (
                    <MiniCards id={index} name={d.name} image={d.image} content={d.content} />
                )
            })}
        
        </Box>
            </Container>
            </React.Fragment>

        
    )
}

export default CatPages

//https://mocki.io/v1/cb554982-b768-4edd-ba43-2ad534f507b7
// https://mocki.io/v1/a9b09d80-5f69-4f0c-811e-2ce7a269626f