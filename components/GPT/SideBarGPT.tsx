import React, { useState } from 'react';
interface Props {
  title: string;
}
const Sidebar: React.FC<Props> = ({ title }) => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <div>
      <button onClick={() => setIsOpen(!isOpen)}>
        {isOpen ? 'Close' : 'Open'} {title}
      </button>
      {isOpen && (
        <div>
          <p>This is the content of the sidebar</p>
        </div>
      )}
    </div>
  );
};
export default Sidebar;