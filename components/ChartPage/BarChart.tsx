import * as React from 'react';
import { Chart, ArcElement } from 'chart.js'
import { Doughnut } from "react-chartjs-2";
import { Box, Card, Typography } from "@mui/material";
import CssBaseline from '@mui/material/CssBaseline';
import Container from "@mui/material/Container"

const data = {
  labels: ["Organic", "Social Media", "Websites"],
  datasets: [
    {
      data: [300, 50],
      backgroundColor: ["#FF6384", "#FFCE56"],
      hoverBackgroundColor: ["#FF6384", "#FFCE56"],
    },
  ],
};
function BarChartPage() {
  Chart.register(ArcElement);
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xl">
        <Box sx={{ bgcolor: '#f0f5ed', height: '100vh', marginLeft: '190px', justifyContent:"center" ,alignItems:"center"}} >
          <Card sx={{ maxWidth: 300, background: "transparent", border: "2px solid pink" }} >
            <Doughnut data={data} width={400} height={400} />
          </Card>
          <Typography>Subscribe</Typography>
        </Box>
      </Container>
    </React.Fragment>
  );
}

export default BarChartPage