import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { Card, CardActionArea, CardMedia, Typography } from '@mui/material';
import ApiDogPage from '../DogPage/ApiDogPage';
import ApiHomePage from './ApiHomePage';

function HomeContent() {
  return (
    <React.Fragment>
       <ApiHomePage/>
    </React.Fragment>
  )
}

export default HomeContent