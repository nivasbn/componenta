import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { Card, CardActionArea, CardMedia, Typography } from '@mui/material';
import HomeData from "./HomeData.json"
interface HomePageProps {
  homeImage:string;
  homeContent:string
}

const SampleHome=(props: HomePageProps)=> {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="xl">
        <Box sx={{ bgcolor: '#f0f5ed', height: '100vh', marginLeft: '190px'}} >
        <Card sx={{ maxWidth: 345, marginLeft:60}}>
            <Box sx={{borderRadius:5 , boxShadow: '5px 10px #0a0a0a'}}>
        <CardMedia
          component="img"
          height="auto"
          image={props.homeImage}
          alt="puppies"
        />
        </Box>
            </Card>
            <Box sx={{ width:800, marginLeft:35, marginTop:5 }}>
            <Typography>
              {props.homeContent}
            </Typography>
            </Box>
           
        </Box>
      </Container>
    </React.Fragment>
  )
}

export default SampleHome