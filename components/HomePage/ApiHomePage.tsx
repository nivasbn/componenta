import React from 'react'
import HomeData from "./HomeData.json"
import SampleHome from './SampleHome'

const ApiHomePage = () => {
  return (
<>
{HomeData.map((d)=>{
    return(
        <>
<SampleHome homeImage={d.homeImage} homeContent={d.homeContent}/>

        </>
    )
})}
</>  )
}

export default ApiHomePage