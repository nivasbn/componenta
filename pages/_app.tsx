import SideBarPage from '@/components/SideBarPage/SideBarPage'
import Navbar from '@/Final/Navbar/Navbar'
import '@/styles/globals.css'
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      {/* <SideBarPage/> */}
      <Navbar />
      <Component {...pageProps} />
    </>

  )
}

