import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { SamplePageService } from "@/API/service/CatagoryPageService"
import sanityClient from '@/client';
import Com from './Com';

  interface SampleProps{
    name:string;
    content:string;
  }
  
 interface ComponentProps{
  loading: boolean;
  users: SampleProps[];
  errorMsg: string;
 }

const Component = () => {
  const [state,setState]=useState<ComponentProps>({
    loading: false,
    users: [] as SampleProps[],
    errorMsg: ''
  });

  const [projectData, setProjectData] = useState(null)
  useEffect(() => {
    setState({ ...state, loading: true })
    SamplePageService.getAllUsers()
        //   .then((res)=>console.log(res.data))
        .then((res) => setState({
            ...state, loading: false, users: res.data
        }))
        .catch(err => setState({
            ...state, loading: false, errorMsg: err.message
        }))
// Eslint-disable-next-line
}, [])



useEffect(() => {
  sanityClient.fetch(`*[_type == "sample"] {
    name,
    content,
  }`)
      .then((data) => setProjectData(data))
      .catch(console.error)

}, [])



const {loading, users, errorMsg} = state






  return (
   <div>
 {users.length > 0 && users.map((d)=>{
  return(
    <div>
      <h1>{d.name}</h1>
      <h1>{d.content}</h1>
      </div>
  )
 })}
 <Com/>
   </div>
  )
}

export default Component


// https://mocki.io/v1/ceb8bfa4-adc6-430b-91af-10b3c52844e1

// *[_type == "sample"] {
//   name,
//   content,
// }