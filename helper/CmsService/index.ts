import sanityClient, { 
    ClientConfig,
    SanityClient,
    FilteredResponseQueryOptions, 
    UnfilteredResponseQueryOptions
} from '@sanity/client';

export interface ICmsService {
    fetch(query: string, params: any): Promise<any>;
}

export class CmsService implements ICmsService {
    private client: SanityClient;

    private clientConfigEnv: ClientConfig = {
        projectId: "p6kats4z",
        dataset: "productions"
    }
    constructor(clientConfig?: ClientConfig) {
        this.client = this.createClient(clientConfig);
    }

    private createClient(clientConfig?: ClientConfig): SanityClient {
        if(!clientConfig) {
            return sanityClient(this.clientConfigEnv);    
        } else {
            return sanityClient(clientConfig);
        }
    }

    fetch<R>(query: string, params: any) {
        return this.client.fetch<R>(query, params);
    }

    fetchFiltered(query: string, params: any | undefined, options: FilteredResponseQueryOptions) {
        return this.client.fetch(query, params, options);
    }

    fetchUnfiltered(query: string, params: any | undefined, options: UnfilteredResponseQueryOptions) {
        return this.client.fetch(query, params, options);
    }
}

export default CmsService;
