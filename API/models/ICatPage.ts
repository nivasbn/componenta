export interface ICatPage{
    id: number;
    name: string;
    image: string;
    content: string;
}