export interface IDogPage{
    id: number;
    name: string;
    image: string;
    content: string;
}