export interface ICatagory{
    id: number;
    name: string;
    image: string;
}