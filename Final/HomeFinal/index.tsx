import Component from '@/helper/Sample/Component'
import React from 'react'
import ContentPage from './ContentPage'
import MarqueeComp from "./MarqueeComp"

const CONTENT=[
  {label:"label1",content:"hj,hmvv,,vhvhhh,h,y"}
]
const HomeFinal = () => {

  return (
    <div>
      <MarqueeComp />
      {CONTENT.map((ds)=>{
        return(
          <ContentPage label={ds.label} content={ds.content}/>
        )
      })}
     <Component/>
    </div>
  )
}

export default HomeFinal