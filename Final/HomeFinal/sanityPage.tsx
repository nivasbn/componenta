import sanityClient from "@sanity/client"

const client = sanityClient({
    projectId: "p6kats4z",
    dataset: "productions",
    useCdn: process.env.NODE_ENV === 'productions'
})

function SanityPage({posts=[]}){
    return(
        <>       
         <ul>
            {posts.map(post=>(
                <li key={post._id}>{post.title}</li>
            ))}
        </ul>
        </>

    )
}

export async function getStaticProps(){
    const posts=await client.fetch(`
    *[_type == 'post']`)


return {
    props: {
        posts
    }
}
}
export default SanityPage