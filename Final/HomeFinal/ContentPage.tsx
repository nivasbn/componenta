import React,{useState,useEffect} from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

import sanityClient from "../../client"


interface ContentProps{
 
    label:string;
    content:string;
}

const ContentPage = (props:ContentProps) => {

  return (
    <Card sx={{ maxWidth: 345 }} >
    <CardActionArea>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {props.label}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {props.content}
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>
  )
}

export default ContentPage