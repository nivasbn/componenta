import { Typography } from '@mui/material'
import React from 'react'
import Marquee from 'react-fast-marquee'
import { Icon } from '@iconify/react';

const MarqueeComp = () => {
    return (
        <Marquee
            speed={40}
            style={{ height: "auto", width: "auto", backgroundColor: "#b2a3d1" }}
            pauseOnHover={true}
            gradient={false}>
            <Icon icon="openmoji:dog-face" />
            <Typography> This website is only for demo, which have for learning purpose,  I have attached the source code with this page kindy check... </Typography>
        </Marquee>
    )
}

export default MarqueeComp