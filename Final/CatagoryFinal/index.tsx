import { Box } from '@mui/material'
import React from 'react'
import ButtonComp from "./ButtonComp"

const CATAGORY_LIST=[
  {text: "Dog",href:"/catagory/dogs"},
  {text: "Cat",href:"/catagory/cats"},
  {text: "Bird",href:"/catagory/birds"}
]
const CatagoryFinal=()=> {
  return (
    <Box sx={{marginLeft:"30%",marginRight:"30%",padding:"30px 0px"}}>
    <ButtonComp/>

    </Box>
  )
}

export default CatagoryFinal