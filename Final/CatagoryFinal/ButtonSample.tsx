import { Box, Button } from '@mui/material'
import React from 'react'
import Link from "next/link";


interface ButtonProps{
  href:string;
  text:string;
}



const ButtonSample = (props:ButtonProps)=> {
  return (
<Link href={props.href}>
        <Button sx={{height:30,width:50,border:"2px solid #6e97d4",marginLeft:"50px"
        }}>{props.text}</Button> 
    </Link>  )
}

export default ButtonSample