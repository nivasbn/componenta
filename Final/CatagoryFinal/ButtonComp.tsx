import { Box, Button } from '@mui/material'
import React from 'react'
import Link from "next/link";
import ButtonSample from "./ButtonSample"

const CATAGORY_LIST=[
  {text: "Dog",href:"/catagory/dogs"},
  {text: "Cat",href:"/catagory/cats"},
  {text: "Bird",href:"/catagory/birds"}
]
const ButtonComp = () => {
  return (
    <Box >
    {CATAGORY_LIST.map((catagory)=>{
      return(
        <ButtonSample key={catagory.text}  text={catagory.text} href={catagory.href}/>
      )
    })}
  </Box>
  )
}

export default ButtonComp