import { Box, Container, CssBaseline } from '@mui/material'
import React,{useState,useEffect} from 'react'
import { ICatagory } from "@/API/models/ICatagory"
import { CatagoryBirdPageService } from "@/API/service/CatagoryPageService"
import MiniCard from './MiniCard'

interface IState{
    loading: boolean;
    users: ICatagory[];
    errorMsg: string;
}
// const Bird = [
//     { id: 1, name: "bird1" },
//     { id: 2, name: "bird2" },
//     { id: 3, name: "bird3" },
//     { id: 4, name: "bird4" },
//     { id: 5, name: "bird4" },
//     { id: 6, name: "bird4" },
//     { id: 7, name: "bird4" },
// ]
const BirdCatagory = () => {
    const [state,setState]=useState<IState>({
        loading: false,
        users: [] as ICatagory[],
        errorMsg: ''
       })

       useEffect(() => {
        setState({ ...state, loading: true })
        CatagoryBirdPageService.getAllUsers()
            //   .then((res)=>console.log(res.data))
            .then((res) => setState({
                ...state, loading: false, users: res.data
            }))
            .catch(err => setState({
                ...state, loading: false, errorMsg: err.message
            }))
    // Eslint-disable-next-line
    }, [])
    const {loading, users, errorMsg} = state
    return (
        <React.Fragment>
            <CssBaseline />
            <Container sx={{ width: "auto", height: "auto" }}>
                <Box sx={{ bgcolor: '#f0f5ed', display: 'inline-flex', flexWrap: "wrap", marginLeft: "20%", marginRight: "20%", marginTop: "60px" }} >
                    {/* <Box sx={{ marginLeft: "10%", marginRight: "10%", backgroundColor: "red", flexDirection: 'row', */}
                    {/* flexWrap: 'wrap'}}> */}
                    {users.length > 0 && users.map((d) => {
                        return (
                            <MiniCard name={d.name} id={d.id} image={d.image} />

                        )
                    })}
                </Box>
            </Container>
        </React.Fragment>
    )
}

export default BirdCatagory

//   https://mocki.io/v1/06fc683e-acac-4e96-a1c8-8061747b6d94