import { Box, Container, CssBaseline } from '@mui/material'
import MiniCard from './MiniCard'
import React,{useState,useEffect} from 'react'
import { ICatagory } from "@/API/models/ICatagory"
import { CatagoryCatPageService } from "@/API/service/CatagoryPageService"

interface IState{
    loading: boolean;
    users: ICatagory[];
    errorMsg: string;
}

const Cat = [
    { id: 1, name: "cat1" },
    { id: 2, name: "cat2" },
    { id: 3, name: "cat3" },
    { id: 4, name: "cat4" },
    { id: 5, name: "cat4" },
    { id: 6, name: "cat4" },
    { id: 7, name: "cat4" },
]
const CatCatagory = () => {
    const [state,setState]=useState<IState>({
        loading: false,
        users: [] as ICatagory[],
        errorMsg: ''
       })

       useEffect(() => {
        setState({ ...state, loading: true })
        CatagoryCatPageService.getAllUsers()
            //   .then((res)=>console.log(res.data))
            .then((res) => setState({
                ...state, loading: false, users: res.data
            }))
            .catch(err => setState({
                ...state, loading: false, errorMsg: err.message
            }))
    // Eslint-disable-next-line
    }, [])
    const {loading, users, errorMsg} = state
    return (
        <React.Fragment>
            <CssBaseline />
            <Container sx={{ width: "auto", height: "auto" }}>
                <Box sx={{ bgcolor: '#f0f5ed', display: 'inline-flex', flexWrap: "wrap", marginLeft: "20%", marginRight: "20%", marginTop: "60px" }} >
                    {/* <Box sx={{ marginLeft: "10%", marginRight: "10%", backgroundColor: "red", flexDirection: 'row', */}
                    {/* flexWrap: 'wrap'}}> */}
                    {users.length > 0 && users.map((d) => {
                        return (
                            <MiniCard name={d.name} id={d.id} image={d.image} />

                        )
                    })}
                </Box>
            </Container>
        </React.Fragment>
    )
}

export default CatCatagory