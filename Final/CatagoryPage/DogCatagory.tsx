import { Box, Container, CssBaseline } from '@mui/material'
import React,{useState,useEffect} from 'react'
import { ICatagory } from "@/API/models/ICatagory"
import { CatagoryPageService } from "@/API/service/CatagoryPageService"
import MiniCard from './MiniCard'

interface IState{
    loading: boolean;
    users: ICatagory[];
    errorMsg: string;
}
// const Dog = [
//     { id: 1, name: "dog1" },
//     { id: 2, name: "dog2" },
//     { id: 3, name: "dog3" },
//     { id: 4, name: "dog4" },
//     { id: 5, name: "dog4" },
//     { id: 6, name: "dog4" },
//     { id: 7, name: "dog4" },
// ]
const DogCatagory = () => {
    const [state,setState]=useState<IState>({
        loading: false,
        users: [] as ICatagory[],
        errorMsg: ''
       })

       useEffect(() => {
        setState({ ...state, loading: true })
        CatagoryPageService.getAllUsers()
            //   .then((res)=>console.log(res.data))
            .then((res) => setState({
                ...state, loading: false, users: res.data
            }))
            .catch(err => setState({
                ...state, loading: false, errorMsg: err.message
            }))
    // Eslint-disable-next-line
    }, [])
    const {loading, users, errorMsg} = state

    return (
        <React.Fragment>
            <CssBaseline />
            <Container sx={{ width: "auto", height: "auto" }}>
                <Box sx={{ bgcolor: '#f0f5ed', display: 'inline-flex', flexWrap: "wrap", marginLeft: "5%", marginRight: "5%", marginTop: "60px" }} >
                    {/* <Box sx={{ marginLeft: "10%", marginRight: "10%", backgroundColor: "red", flexDirection: 'row', */}
                    {/* flexWrap: 'wrap'}}> */}
                    {users.length > 0 && users.map((d) => {
                        return (
                            <MiniCard name={d.name} id={d.id} image={d.image} />

                        )
                    })}
                </Box>
            </Container>
        </React.Fragment>
    )
}

export default DogCatagory