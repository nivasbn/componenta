import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

interface MiniCardProps {
  name: string;
  image:string;
  id: number;
}
const MiniCard = (props: MiniCardProps) => {
  return (
    <Card sx={{ width: 250, marginTop: 3, marginLeft: "5%" }} key={props.id}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="180"
          width="240"
          image={props.image}
          alt={props.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="div">
            {props.name}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default MiniCard